ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_python:3.8.2 AS opt_python

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_tools:3.11.3 AS build

COPY --from=opt_python /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV PKG_CONFIG_PATH /opt/pkgconfig/
ENV LD_LIBRARY_PATH /opt/lib/

RUN apk update && \
    apk add libffi-dev openssl-dev

RUN mkdir -p /tmp/scratch/build && \
    cd /tmp/scratch && wget -t 1 http://localhost:8000/dxpy-0.290.1.tar.gz || \
                       wget -t 3 https://files.pythonhosted.org/packages/47/18/b143d60134ec2aee2cc3e5c5fa2a53067013181c2856944c817bfc062f1c/dxpy-0.290.1.tar.gz && \
    cd /tmp/scratch/ && tar -xvf dxpy-0.290.1.tar.gz && \
    cd /tmp/scratch/dxpy-0.290.1/ && python -m pip install ./ && \
    cd / && rm -rf /tmp/scratch/

#RUN which dx && false
#RUN ls /usr/lib/python3.8/site-packages/ && false
FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

COPY --from=build /opt/bin/ /opt/bin/
COPY --from=build /opt/lib/ /opt/lib/

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

ENTRYPOINT [ "/opt/bin/dx" ]
#RUN dx
#RUN false